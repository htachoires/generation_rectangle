package fr.htachoires;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author hugot
 */
public class main extends Application {

    static final int LENGTH = 700;
    static final int HEIGHT = 700;

    static final int INTERVALLE = 4;
    static final int DEPLACEMENT_ANIMATION = 1000;

    static Timeline dessinerRectangle;
    static Timeline animationRectangle;
    static Timeline dessinerLigne;
    static Timeline effacerFormes;

    static int nbRectangles;
    static int nbLignes;
    static int nbRectangleApresCollision;
    static int node;

    static ArrayList<RectangleJeu> rectangles;
    static HashSet<RectangleJeu> rectanglesASupprimer;
    static HashSet<Line> lignes;

    static int generationR;
    static int generationV;
    static int generationB;

    static RectangleJeu rectangleCentre;
    static AnchorPane root;
    static Random rand;

    @Override
    public void start(Stage primaryStage) {
        /*---initialisation---*/
        nbRectangles = LENGTH + HEIGHT;

        root = new AnchorPane();
        rand = new Random();
        node = nbRectangles;
        rectangles = new ArrayList<>();
        rectanglesASupprimer = new HashSet<>();
        lignes = new HashSet<>();

        dessinerRectangle = new Timeline(new KeyFrame(Duration.millis(INTERVALLE), se -> {
            int largeur = rand.nextInt(LENGTH / 25) + LENGTH / 40;
            int hauteur = rand.nextInt(HEIGHT / 25) + HEIGHT / 40;
            int posX = rand.nextInt(LENGTH);
            int posY = rand.nextInt(HEIGHT);

            while (posX + largeur > LENGTH) {
                posX = rand.nextInt(LENGTH);
            }
            while (posY + hauteur > HEIGHT) {
                posY = rand.nextInt(HEIGHT);
            }
            RectangleJeu rect = new RectangleJeu(posX, posY, largeur, hauteur);
            rect.setFill(new Color(rand.nextDouble() * generationR,
                    rand.nextDouble() * generationV,
                    rand.nextDouble() * generationB, 1));
            rectangles.add(rect);
            root.getChildren().add(rect);
        }));

        dessinerRectangle.setCycleCount(nbRectangles);

        dessinerRectangle.setOnFinished(e -> {
            rectangleCentre = (RectangleJeu) root.getChildren().get(0);
            for (RectangleJeu r1 : rectangles) {
                if (centre(r1)) {
                    rectangleCentre = r1;
                }
            }
            rectangleCentre.toBack();
            rectangles.remove(rectangleCentre);
            rectangles.add(0, rectangleCentre);
            for (RectangleJeu r : rectangles) {
                if (!rectanglesASupprimer.contains(r)) {
                    for (RectangleJeu r2 : rectangles) {
                        if (!r.equals(r2) && !rectanglesASupprimer.contains(r2)) {
                            if (estCollision(r, r2)) {
                                rectanglesASupprimer.add(r2);
                            }
                        }
                    }
                }
            }

            if (!rectanglesASupprimer.isEmpty()) {
                Timeline effaceCollision = new Timeline(new KeyFrame(Duration.millis(INTERVALLE), event -> {
                    if (!rectanglesASupprimer.isEmpty()) {
                        RectangleJeu r = rectanglesASupprimer.iterator().next();

                        root.getChildren().remove(r);
                        rectanglesASupprimer.remove(r);
                        rectangles.remove(r);
                        node--;
                    }
                }));
                effaceCollision.setCycleCount(rectanglesASupprimer.size());
                effaceCollision.setDelay(Duration.seconds(1));
                effaceCollision.setOnFinished(event -> {
                    nbRectangleApresCollision = node;
                    animationRectangle.setCycleCount(nbRectangleApresCollision);
                    animationRectangle.play();
                });

                effaceCollision.play();

            } else {
                nbRectangleApresCollision = node;
                animationRectangle.setCycleCount(nbRectangles);
                effacerFormes.setCycleCount(nbRectangles);
                animationRectangle.play();
            }

        });

        animationRectangle = new Timeline(new KeyFrame(Duration.millis(5), e -> {
            animer(--node);
        }));

        effacerFormes = new Timeline(new KeyFrame(Duration.millis(INTERVALLE), e -> {
            if (!root.getChildren().isEmpty()) {
                root.getChildren().remove(root.getChildren().size() - 1);
            }
        }));

        effacerFormes.setOnFinished(e -> {
            rectanglesASupprimer.clear();
            rectangles.clear();
            lignes.clear();
            node = nbRectangles;
            root.setDisable(false);
        });

        effacerFormes.setDelay(Duration.seconds(1));

        root.setOnMouseClicked(e -> {
            if (!root.isDisable()) {
                root.setDisable(true);
                root.getChildren().clear();
                generationR = rand.nextInt(2);
                generationV = rand.nextInt(2);
                generationB = rand.nextInt(2);
                if (generationR == 0 && generationV == 0 && generationB == 0) {
                    switch (rand.nextInt(3)) {
                        case 0:
                            generationR = 1;
                            break;
                        case 1:
                            generationV = 1;
                            break;
                        case 2:
                            generationB = 1;
                            break;
                    }
                }
                dessinerRectangle.play();
            }
        });

        dessinerLigne = new Timeline(new KeyFrame(Duration.seconds(1), e -> {
            Timeline afficherLigne = new Timeline(new KeyFrame(Duration.millis(INTERVALLE), ev -> {
                if (!lignes.isEmpty()) {
                    Line line = lignes.iterator().next();
                    root.getChildren().add(line);
                    lignes.remove(line);
                }
            }));
            nbLignes = lignes.size();
            afficherLigne.setCycleCount(nbLignes);
            afficherLigne.setOnFinished(event -> {
                effacerFormes.setCycleCount(nbRectangleApresCollision + nbLignes);
                effacerFormes.play();
            });
            afficherLigne.play();
        }));

        primaryStage.setTitle("Rectangle !!!");
        primaryStage.setScene(new Scene(root, LENGTH, HEIGHT));
        primaryStage.getIcons().add(new Image("/fr/assets/images/icon.png"));
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void animer(int rect) {
        Timeline ani = new Timeline(new KeyFrame(Duration.millis(250), (ev) -> {
            RectangleJeu r = (RectangleJeu) root.getChildren().get(rect);
            TranslateTransition trans = new TranslateTransition(Duration.millis(500), r);
            int posX = rand.nextInt(DEPLACEMENT_ANIMATION) - DEPLACEMENT_ANIMATION / 2;
            int posY = rand.nextInt(DEPLACEMENT_ANIMATION) - DEPLACEMENT_ANIMATION / 2;

            while (r.getWidth() + r.posX + posX > LENGTH || r.posX + posX < 0) {
                posX = rand.nextInt(DEPLACEMENT_ANIMATION) - DEPLACEMENT_ANIMATION / 2;
            }

            while (r.getHeight() + r.posY + posY > HEIGHT || r.posY + posY < 0) {
                posY = rand.nextInt(DEPLACEMENT_ANIMATION) - DEPLACEMENT_ANIMATION / 2;
            }
            trans.setToX(posX);
            trans.setToY(posY);
            trans.setAutoReverse(true);
            trans.setCycleCount(4);
            trans.play();
            if (rect == node) {
                trans.setOnFinished(e -> {
                    rectangleCentre.toBack();
                    for (RectangleJeu r1 : rectangles) {
                        if (!r1.equals(rectangleCentre)) {
                            Line line = new Line(rectangleCentre.posX + rectangleCentre.getWidth() / 2,
                                    rectangleCentre.posY + rectangleCentre.getHeight() / 2,
                                    r1.posX + r1.getWidth() / 2,
                                    r1.posY + r1.getHeight() / 2);
                            line.setStrokeWidth(3);
                            line.setStroke(new Color(
                                    rand.nextDouble() * generationR,
                                    rand.nextDouble() * generationV,
                                    rand.nextDouble() * generationB, 1)
                            );
                            lignes.add(line);
                        }
                    }
                    dessinerLigne.play();
                });
            }
        }));
        ani.play();
    }

    private static boolean estCollision(RectangleJeu r1, RectangleJeu r2) {
        return r1.getHeight() + r1.posY > r2.posY
                && r1.posY < r2.posY + r2.getHeight()
                && r1.posX < r2.posX + r2.getWidth()
                && r1.getWidth() + r1.posX > r2.posX;
    }

    private static boolean centre(RectangleJeu r) {
        //calcule des distances
        return Math.sqrt(Math.pow((r.posX + r.getWidth() / 2) - LENGTH / 2, 2)
                + Math.pow((r.posY + r.getHeight() / 2) - HEIGHT / 2, 2))
                < Math.sqrt(Math.pow((rectangleCentre.posX + rectangleCentre.getWidth() / 2) - LENGTH / 2, 2)
                        + Math.pow((rectangleCentre.posY + rectangleCentre.getHeight() / 2) - HEIGHT / 2, 2));
    }

    /*------------------------------------------------------------------------*/
    public class RectangleJeu extends Rectangle {

        public double posX;

        public double posY;

        public Line lineG;

        public RectangleJeu(double x, double y, double width, double height) {
            super(x, y, width, height);
            posX = x;
            posY = y;
        }

        @Override
        public String toString() {
            return String.format("posX=%d, posY=%d, hauteur=%d, largeur=%d, estLigne=%b",
                    (int) posX, (int) posY, (int) getHeight(), (int) getWidth(), isLine());
        }

        public boolean isLine() {
            return lineG != null;
        }

        public void setLine(Line line) {
            this.lineG = line;
        }

    }

    public static void main(String[] args) {
        launch(args);
    }
}
