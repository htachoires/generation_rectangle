## Generation de rectangle

Pour découvrir les timelines en java j'ai réalisé ce petit programme pour mettre en application ce qu'elles proposent.

### Décomposition du programme

* génération aléatoire de rectangles qui ne débordent pas de la fenetre
* Selection du rectangle le plus au centre de la fenetre
* Parcours des rectangles pour supprimer les collisions entre rectangles
* Des mouvement aléatoires pour chaque rectangles sans sortire de la fenetre
* des lignes sont reliées à chaque rectangles depuis le rectangles centre
* Suppression des rectangles et des lignes pour recommencer une génération